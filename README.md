Start
https://gitlab.com/insafkamaletdinov/docker-compose.git
cd docker-compose
git clone https://github.com/mbaran0v/python-sample-app
docker-compose up -d

Test
http://127.0.0.1:5000/api/user

Result
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 3
Server: Werkzeug/0.15.5 Python/3.8.6
Date: Tue, 15 Dec 2020 18:19:57 GMT

[]
